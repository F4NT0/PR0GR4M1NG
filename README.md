# Bem vindo ao Website de Informações do Fanto

<center>
  <img src="img/terminal.gif" width="300">
</center>

<center>
    B3M V1ND0 40 C0NH3C1M3NT0
</center>

---

<center>
<code style="color:yellow">GLOSSÁRIO</code>
</center>

---

**PÁGINAS DE LINGUAGENS DE PROGRAMAÇÃO**

Link para Página|Informações|Imagem
|---|---|---|
[Página inicial de Java](pages/prog_java/home.md)|Estudos da Linguagem Java|<img src="img/java-icon.jpg" width="100">
[Página inicial de C](pages/prog_c/home.md)| Estudos da Linguagem C|<img src="img/c-icon.png" width="100">
[Página inicial de Python](pages/prog_python/home.md)| Estudos da Linguagem Python|<img src="img/python-icon.jpg" width="85">
[Página inicial de Javascript](pages/prog_js/home.md)| Estudos da Linguagem Javascript|<img src="img/js-icon.jpeg" width="80">

**PÁGINAS DE TECNOLOGIAS APRENDIDAS**

Link para Página|Informações|Imagem
|---|---|---|
[Página inicial de Docker](pages/org_docker/home.md)| Estudos da Tecnologia Docker|<img src="img/docker-icon.png" width="100">


**MEXENDO COM TERMINAIS**

Link para Página|Informações|Imagem
|---|---|---|
[Mexendo com o Shell CLI do Unix](pages/term_unix/home.md)| Estudos do Terminal do Unix|<img src="img/linux-terminal.png" width="100">
[Mexendo com o Shell CLI Prompt de Comando do Windows](pages/term_prompt/home.md)| Estudos do Terminal Prompt de Comando|<img src="img/win-terminal.png" width="100">
[Mexendo com o Shell CLI Powershell do Windows](pages/term_powershell/home.md)| Estudos do Terminal Prompt de Comando|<img src="img/power-terminal.png" width="100">