# Como funciona a organização do Diretório pages

* Este diretório tem os seguintes tipos de Diretórios:
    * `prog_` : São diretórios com informações de uma linguagem especifica
    * `org_`  : São diretórios com informações de organização de uma tecnologia
    * `tut_`  : São diretórios onde possui o tutorial de alguma coisa
    * `tech_` : São diretórios que possuem informações de uma tecnologia especifica
    * `term_` : São diretórios com informações de Terminais de diferentes Sistemas Operacionais 

# Dentro de cada Diretório tem dois arquivos excenssiais:

Nome Arquivo|Utilidade
|---|---|
README.md| Arquivo de organização do Diretório Especifico
home.md| Arquivo que serve como Menu Principal de cada diretório