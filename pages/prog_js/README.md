# Diretório de informações da Linguagem Javascript

* **Controle das Páginas deste Diretório**

---

Glossário de Emojis:

* :heavy_check_mark: = Página Adicionada
* :heavy_plus_sign: = Página que precisa ser adicionada
* :x: = Página que precisa ser criada

---

[**`Básico da Linguagem`**]()

Nome da Página|Status
|---|---|
basico.md|:heavy_check_mark:
math.md|:heavy_check_mark:
dec-rep.md|:heavy_check_mark:
functions.md|:heavy_check_mark:
