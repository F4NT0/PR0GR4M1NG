# Diretório de informações da Linguagem Python

* **Controle das Páginas deste Diretório**

---

Glossário de Emojis:

* :heavy_check_mark: = Página Adicionada
* :heavy_plus_sign: = Página que precisa ser adicionada
* :x: = Página que precisa ser criada

---

[**`Básico da Linguagem`**]()

Nome da Página|Status
|---|---|
home.md|:heavy_check_mark:
tipos-dados.md|:heavy_check_mark:
estr-decisoes.md|:heavy_check_mark:
input.md|:heavy_check_mark:
for.md|:heavy_check_mark:
while.md|:heavy_check_mark:
listas.md|:heavy_check_mark:
funcoes.md|:heavy_check_mark:
arquivos.md|:heavy_check_mark:
try-catch.md|:heavy_check_mark:
dicionarios.md|:heavy_check_mark: