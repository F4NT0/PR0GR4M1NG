# Diretório de informações da Linguagem C

* **Controle das Páginas deste Diretório**

---

Glossário de Emojis:

* :heavy_check_mark: = Página Adicionada
* :heavy_plus_sign: = Página que precisa ser adicionada
* :x: = Página que precisa ser criada

---

[**`Básico da Linguagem`**]()

Nome da Página|Status
|---|---|
compilador.md|:heavy_check_mark:
estrutura-basica.md| :heavy_check_mark:
tipos-dados.md| :heavy_check_mark:
estrutura-decisao.md|:heavy_check_mark:
estrutura-repeticao.md|:heavy_check_mark:
estrutura-dados.md| :heavy_check_mark:
strings.md| :heavy_check_mark:
funcoes.md| :heavy_check_mark:
structs.md| :heavy_check_mark:
bits-manipulacao.md| :x:
escrevendo-arquivo.md| :x:
lendo-arquivo.md|:x:
modularizacao.md|:x:
ponteiros.md |:x:
aloc-memoria.md|:x: