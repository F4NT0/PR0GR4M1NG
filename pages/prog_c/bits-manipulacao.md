[[Página Inicial](../prog_c/home.md)]

# Manipulação de Bits em C

Manipulação de Bits é o ato de algoritimicamente manipular Bits ou outros pedaços de dados menores que uma palavra.


### Revisando Bases Numéricas

* Dados numéricos podem ser representados através de várias bases:
  * Decimal
  * Hexadecimal
  * Binário

* Uma base numérica é apenas uma forma de interpretar os dados, não mudando o valor armazenado
* Todo número apresentado como Binário é o mesmo valor como Decimal, sem sofrer mudanças bruscas:
* 0001 é o mesmo que 1 só que binário

#### Decimal

* Bom para humanos, péssimos para Desenvolvedores
* Decimal é muito ruim de converter para outros valores, então nãoé eficiente para Desenvolvimento.

Decimal é bom para <code style="color : green">Humanos</code>, Péssimo para <code style="color : yellow">Desenvolvedores</code>.

#### Binários de Base 2

Binário é bom para <code style="color : lightblue">Computadores</code> e péssimo para <code style="color : red">Todo Mundo</code>


