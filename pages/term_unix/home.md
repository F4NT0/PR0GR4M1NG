[[**Página inicial**](https://f4nt0.github.io/PR0GR4M1NG)]

---
# Página inicial sobre Terminais Unix
---

---

<center>
    <code style="color : gold">GLOSSÁRIO</code>
</center>

---


<center>
    <code style="color : greenyellow">Sistema Linux Mint</code>
</center>

<center>
    <img src="../../img/linux-mint.png" width="100">
</center>

**Páginas do Linux Mint**

Link da Página|Resumo
|---|---|
[Comandos do Terminal do Linux Mint](../term_unix/mint-command.md)| Lista de Comandos para utilizar no Terminal do Linux Mint
[Configurando o seu Terminal](../term_unix/config-screen.md)| Como configurar a sua tela do Terminal
