# Diretório de Informações sobre o Shell CLI UNIX

* **Controle das Páginas deste Diretório**

---

Glossário de Emojis:

* :heavy_check_mark: = Página Adicionada
* :heavy_plus_sign: = Página que precisa ser adicionada
* :heavy_plus_sign: = Página que precisa ser criada

---

[**`Páginas de Comandos`**]()

Nome da Página|Status
|---|---|
mint-command.md|:heavy_check_mark:
comandos-ubuntu.md|:heavy_plus_sign:
comandos-arch.md|:heavy_plus_sign:
comandos-debian.md|:heavy_plus_sign:

[**`Páginas de Gerenciamento Linux`**]()

Nome da Página|Status
|---|---|
configurando-cores.md|:heavy_plus_sign:
configurando-shell.md|:heavy_plus_sign:
configurando-ps1.md|:heavy_plus_sign:
colocando-mensagem-terminal.md|:heavy_plus_sign:
ansi-cores.md|:heavy_plus_sign:

[**`Páginas de Utilidades pelo terminal`**]()

Nome da Página|Status
|---|---|
