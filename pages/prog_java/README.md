# Diretório de Informações da Linguagem Java

* **Controle das Páginas deste Diretório**

---

Glossário de Emojis:

* :heavy_check_mark: = Página Adicionada
* :heavy_plus_sign: = Página que precisa ser adicionada
* :x: = Página que precisa ser criada

---

[**`Básico da Linguagem`**]()

_Em manutenção_

[**`Orientação a Objetos`**]()

Nome da Página|Status
|---|---|
home.md|:heavy_check_mark: 
class_model.md|:heavy_check_mark:
genericos.md|:heavy_check_mark:
listas.md|:heavy_check_mark:
sobrecarga.md|:heavy_check_mark:
interface.md|:heavy_check_mark:
heranca.md|:heavy_check_mark:
classes_abstratas.md|:heavy_check_mark:
polimorfismo.md|:heavy_check_mark:
uml.md|:heavy_check_mark:
junit.md|:heavy_check_mark:
exceptions_teoria.md|:heavy_check_mark:
exceptions.md|:heavy_check_mark:
data_flux.md|:heavy_check_mark:
writing_file.md|:heavy_check_mark:
reading_file.md|:heavy_check_mark:
